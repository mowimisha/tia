<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Maatwebsite\Excel\Concerns\Exportable;

class Compliance extends Model
{
    use Notifiable, Exportable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 'registration_no', 'tax_registration_no', 'vat_registration_no', 'contact_person',
        'contact_person_email', 'contact_person_mobile',
    ];
}
