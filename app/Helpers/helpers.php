<?php

if (!function_exists('divnum')) {

    function divnum($numerator, $denominator)
    {
        return $denominator == 0 ? 0 : number_format(((float)$numerator / $denominator), 1);
    }
}
