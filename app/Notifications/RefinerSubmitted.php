<?php

namespace App\Notifications;

use App\Refiner;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RefinerSubmitted extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $refiner = new Refiner();
        return (new MailMessage)
            ->greeting('Hello!')
            ->subject('TIA360 Registration')
            ->from('refiner@tia360.africa', 'TIA360 Africa')
            ->line('You have successfully Registered with TIA360 and Submitted your Refiner.')
            ->line('We will get in touch with you with more details.')
            ->line('Thank you for signing up with TIA360!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}


// MAIL_MAILER=smtp
// MAIL_HOST=smtp.gmail.com
// MAIL_PORT=587
// MAIL_USERNAME=michaelmagero2@gmail.com
// MAIL_PASSWORD=@tamu1614
// MAIL_ENCRYPTION=tls
// MAIL_FROM_ADDRESS=michaelmagero2@gmail.com
// MAIL_FROM_NAME="${APP_NAME}"
