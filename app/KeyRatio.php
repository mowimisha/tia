<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Maatwebsite\Excel\Concerns\Exportable;

class KeyRatio extends Model
{
    use Notifiable, Exportable;

    protected $table = 'key_ratios';
}
