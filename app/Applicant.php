<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Maatwebsite\Excel\Concerns\Exportable;

class Applicant extends Model
{
    use Exportable;

    // /**
    //  * Route notifications for the mail channel.
    //  *
    //  * @param  \Illuminate\Notifications\Notification  $notification
    //  * @return array|string
    //  */
    // public function routeNotificationForMail($notification)
    // {
    //     // Return email address only...
    //     return $this->email;

    //     // Return email address and name...
    //     return [$this->email => $this->firstname];
    // }
}
