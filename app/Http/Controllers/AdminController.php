<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     *
     * Return admin index view
     *
     * @param Request $request
     * @return applicants
     * @return tier1id
     * @return tier2id
     **/
    public function index(Request $request)
    {
        $user = $request->user()->role;
        $tier1_id = DB::table('key_ratios')->where('tier', 1)->count();
        $tier2_id = DB::table('key_ratios')->where('tier', 2)->count();

        switch ($user) {
            case 0:
                return view('admin.index')
                    ->with('applicants', DB::table('applicants')->get())
                    ->with('tier1', $tier1_id)
                    ->with('tier2', $tier2_id);
                break;
            case 1:
                return view('admin.index')
                    ->with('applicants', DB::table('applicants')->get())
                    ->with('tier1', $tier1_id)
                    ->with('tier2', $tier2_id);
                break;
            default:
                break;
        }
    }
}
