<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Background;
use App\Compliance;
use App\Financial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    /**
     *
     * Get a list of all applicants
     *
     * @param Request $request
     *
     * @return applicants
     * @return compliances
     * @return background
     * @return finacials
     * @return ratios
     **/
    public function index()
    {
        return view('admin.applications.index')
            ->with('applicants', Applicant::latest('id')->paginate(15))
            ->with('compliances', Compliance::latest('id')->paginate(15))
            ->with('backgrounds', Background::latest('id')->paginate(15))
            ->with('financials', Financial::latest('id')->paginate(15));
    }

    /**
     *
     * show applicants registration details and calculation results (ratios)
     *
     *
     * @param int $id
     *
     * @return applicants
     * @return compliances
     * @return background
     * @return finacials
     * @return ratios
     **/
    public function show($id)
    {
        $applicant = DB::table('applicants')->where('id', $id)->get();
        $compliance = DB::table('compliances')->where('id', $id)->get();
        $background = DB::table('backgrounds')->where('id', $id)->get();
        $financial = DB::table('financials')->where('id', $id)->get();
        $ratios = DB::table('key_ratios')->where('id', $id)->get();

        return view('admin.applications.show')
            ->with('applicants', $applicant)
            ->with('compliances', $compliance)
            ->with('backgrounds', $background)
            ->with('financials', $financial)
            ->with('ratios', $ratios);
    }
}
