<?php

namespace App\Http\Controllers;

use App\KeyRatio;
use App\Applicant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Notifications\RefinerSubmitted;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class RefinerController extends Controller
{
    /**
     * Return tier1 index list table
     * @return applicants,
     * @return ids (keyratio),
     **/
    public function tier_one()
    {
        return view('admin.tier1.index')
            ->with('applicants', Applicant::get())
            ->with('ids', KeyRatio::where('tier', 1)->get());
    }

    /**
     * Return tier2 index list table
     * @return applicants,
     * @return ids (keyratio),
     **/
    public function tier_two()
    {
        return view('admin.tier2.index')
            ->with('applicants', Applicant::get())
            ->with('ids', KeyRatio::where('tier', 2)->get());
    }

    /*********************  FORM INPUT VALUES SUBMISSIONS */
    /**
     * insert applicants into db table
     *
     * @param Request $request
     **/
    public function submit_applicant($request)
    {
        DB::table('applicants')->insert([
            'firstname' => $request->firstname, 'lastname' => $request->lastname, 'race' => $request->race, 'nationality' =>  $request->nationality,
            'email' => $request->email, 'phone' => $request->phone, 'business_status' => $request->business_status
        ]);
    }

    /**
     * insert company compliance details into db table
     *
     * @param Request $request
     **/
    public function submit_compliance($request)
    {
        DB::table('compliances')->insert([
            'company_name' => $request->company_name, 'registration_no' => $request->registration_no, 'tax_registration_no' => $request->tax_registration_no,
            'vat_registration_no' => $request->vat_registration_no, 'contact_person' => $request->contact_person,
            'contact_person_email' => $request->contact_person_email, 'contact_person_mobile' => $request->contact_person_mobile
        ]);
    }

    /**
     * insert company background details into db
     *
     * @param Request $request
     **/
    public function submit_background($request)
    {
        DB::table('backgrounds')->insert([
            'shareholder_name' => $request->shareholder_name, 'shareholder_id' => $request->shareholder_id,
            'entity_name' => $request->entity_name, 'entity_registration_number' => $request->entity_registration_number,
            'business_status' => $request->business_status, 'individual_shareholder_percentage' => $request->individual_shareholder_percentage,
            'entity_shareholder_percentage' => $request->entity_shareholder_percentage, 'management_company_name' => $request->management_company_name,
            'management_registration_number' => $request->management_registration_number, 'management_company_position' => $request->management_company_position,
            'permanent_staff_no' => $request->permanent_staff_no, 'temporary_staff_no' => $request->temporary_staff_no,
            'business_location_province' => $request->business_location_province, 'business_nearest_city_location' => $request->business_nearest_city_location,
            'project_actual_location_area' => $request->project_actual_location_area, 'business_description' => $request->business_description,
            'business_goto_strategy' => $request->business_goto_strategy, 'business_competitive_advantage' => $request->business_competitive_advantage,
            'business_areas_of_improvement' => $request->business_areas_of_improvement,
            'business_strategic_opportunities' => $request->business_strategic_opportunities,
            'business_financial_year_end' => $request->business_financial_year_end, 'business_accountant_name' => $request->business_accountant_name
        ]);
    }

    /**
     * insert financials into db
     *
     * @param Request $request
     **/
    public function submit_financial($request)
    {
        DB::table('financials')->insert([
            'is_revenue_py3' => $request->is_revenue_py3, 'is_revenue_py2' => $request->is_revenue_py2, 'is_revenue_py1' => $request->is_revenue_py1,
            'is_revenue_ytd' => $request->is_revenue_ytd, 'is_revenue_fp5' => $request->is_revenue_fp5, 'is_revenue_fp4' => $request->is_revenue_fp4,
            'is_revenue_fp3' => $request->is_revenue_fp3, 'is_revenue_fp2' => $request->is_revenue_fp2, 'is_revenue_fp1' => $request->is_revenue_fp1,
            'is_cost_of_sales_py3' => $request->is_cost_of_sales_py3, 'is_cost_of_sales_py2' => $request->is_cost_of_sales_py2,
            'is_cost_of_sales_py1' => $request->is_cost_of_sales_py1, 'is_cost_of_sales_ytd' => $request->is_cost_of_sales_ytd,
            'is_cost_of_sales_fp5' => $request->is_cost_of_sales_fp5, 'is_cost_of_sales_fp4' => $request->is_cost_of_sales_fp4,
            'is_cost_of_sales_fp3' => $request->is_cost_of_sales_fp3, 'is_cost_of_sales_fp2' => $request->is_cost_of_sales_fp2,
            'is_cost_of_sales_fp1' => $request->is_cost_of_sales_fp1, 'is_gross_profit_py3' => $request->is_gross_profit_py3,
            'is_gross_profit_py2' => $request->is_gross_profit_py2, 'is_gross_profit_py1' => $request->is_gross_profit_py1,
            'is_gross_profit_ytd' => $request->is_gross_profit_ytd, 'is_gross_profit_fp5' => $request->is_gross_profit_fp5,
            'is_gross_profit_fp4' => $request->is_gross_profit_fp4, 'is_gross_profit_fp3' => $request->is_gross_profit_fp3,
            'is_gross_profit_fp2' => $request->is_gross_profit_fp2, 'is_gross_profit_fp1' => $request->is_gross_profit_fp1,
            'is_operating_expenses_py3' => $request->is_operating_expenses_py3, 'is_operating_expenses_py2' => $request->is_operating_expenses_py2,
            'is_operating_expenses_py1' => $request->is_operating_expenses_py1, 'is_operating_expenses_ytd' => $request->is_operating_expenses_ytd,
            'is_operating_expenses_fp5' => $request->is_operating_expenses_fp5, 'is_operating_expenses_fp4' => $request->is_operating_expenses_fp4,
            'is_operating_expenses_fp3' => $request->is_operating_expenses_fp3, 'is_operating_expenses_fp2' => $request->is_operating_expenses_fp2,
            'is_operating_expenses_fp1' => $request->is_operating_expenses_fp1, 'is_profit_before_tax_py3' => $request->is_profit_before_tax_py3,
            'is_profit_before_tax_py2' => $request->is_profit_before_tax_py2, 'is_profit_before_tax_py1' => $request->is_profit_before_tax_py1,
            'is_profit_before_tax_ytd' => $request->is_profit_before_tax_ytd, 'is_profit_before_tax_fp5' => $request->is_profit_before_tax_fp5,
            'is_profit_before_tax_fp4' => $request->is_profit_before_tax_fp4, 'is_profit_before_tax_fp3' => $request->is_profit_before_tax_fp3,
            'is_profit_before_tax_fp2' => $request->is_profit_before_tax_fp2, 'is_profit_before_tax_fp1' => $request->is_profit_before_tax_fp1,
            'is_tax_py3' => $request->is_tax_py3, 'is_tax_py2' => $request->is_tax_py2, 'is_tax_py1' => $request->is_tax_py1,
            'is_tax_ytd' => $request->is_tax_ytd, 'is_tax_fp5' => $request->is_tax_fp5, 'is_tax_fp4' => $request->is_tax_fp4,
            'is_tax_fp3' => $request->is_tax_fp3, 'is_tax_fp2' => $request->is_tax_fp2, 'is_tax_fp1' => $request->is_tax_fp1,
            'is_profit_after_tax_py3' => $request->is_profit_after_tax_py3, 'is_profit_after_tax_py2' => $request->is_profit_after_tax_py2,
            'is_profit_after_tax_py1' => $request->is_profit_after_tax_py1, 'is_profit_after_tax_ytd' => $request->is_profit_after_tax_ytd,
            'is_profit_after_tax_fp5' => $request->is_profit_after_tax_fp5, 'is_profit_after_tax_fp4' => $request->is_profit_after_tax_fp4,
            'is_profit_after_tax_fp3' => $request->is_profit_after_tax_fp3, 'is_profit_after_tax_fp2' => $request->is_profit_after_tax_fp2,
            'is_profit_after_tax_fp1' => $request->is_profit_after_tax_fp1, 'is_depreciation_py3' => $request->is_depreciation_py3,
            'is_depreciation_py2' => $request->is_depreciation_py2, 'is_depreciation_py1' => $request->is_depreciation_py1,
            'is_depreciation_ytd' => $request->is_depreciation_ytd, 'is_depreciation_fp5' => $request->is_depreciation_fp5,
            'is_depreciation_fp4' => $request->is_depreciation_fp4, 'is_depreciation_fp3' => $request->is_depreciation_fp3,
            'is_depreciation_fp2' => $request->is_depreciation_fp2, 'is_depreciation_fp1' => $request->is_depreciation_fp1,
            'is_interest_expense_py3' => $request->is_interest_expense_py3, 'is_interest_expense_py2' => $request->is_interest_expense_py2,
            'is_interest_expense_py1' => $request->is_interest_expense_py1, 'is_interest_expense_ytd' => $request->is_interest_expense_ytd,
            'is_interest_expense_fp5' => $request->is_interest_expense_fp5, 'is_interest_expense_fp4' => $request->is_interest_expense_fp4,
            'is_interest_expense_fp3' => $request->is_interest_expense_fp3, 'is_interest_expense_fp2' => $request->is_interest_expense_fp2,
            'is_interest_expense_fp1' => $request->is_interest_expense_fp1, 'bs_total_assets_py3' => $request->bs_total_assets_py3,
            'bs_total_assets_py2' => $request->bs_total_assets_py2, 'bs_total_assets_py1' => $request->bs_total_assets_py1,
            'bs_total_assets_ytd' => $request->bs_total_assets_ytd, 'bs_total_assets_fp5' => $request->bs_total_assets_fp5,
            'bs_total_assets_fp4' => $request->bs_total_assets_fp4, 'bs_total_assets_fp3' => $request->bs_total_assets_fp3,
            'bs_total_assets_fp2' => $request->bs_total_assets_fp2, 'bs_total_assets_fp1' => $request->bs_total_assets_fp1,
            'bs_fixed_assets_py3' => $request->bs_fixed_assets_py3, 'bs_fixed_assets_py2' => $request->bs_fixed_assets_py2,
            'bs_fixed_assets_py1' => $request->bs_fixed_assets_py1, 'bs_fixed_assets_ytd' => $request->bs_fixed_assets_ytd,
            'bs_fixed_assets_fp5' => $request->bs_fixed_assets_fp5, 'bs_fixed_assets_fp4' => $request->bs_fixed_assets_fp4,
            'bs_fixed_assets_fp3' => $request->bs_fixed_assets_fp3, 'bs_fixed_assets_fp2' => $request->bs_fixed_assets_fp2,
            'bs_fixed_assets_fp1' => $request->bs_fixed_assets_fp1, 'bs_current_assets_py3' => $request->bs_current_assets_py3,
            'bs_current_assets_py2' => $request->bs_current_assets_py2, 'bs_current_assets_py1' => $request->bs_current_assets_py1,
            'bs_current_assets_ytd' => $request->bs_current_assets_ytd, 'bs_current_assets_fp5' => $request->bs_current_assets_fp5,
            'bs_current_assets_fp4' => $request->bs_current_assets_fp4, 'bs_current_assets_fp3' => $request->bs_current_assets_fp3,
            'bs_current_assets_fp2' => $request->bs_current_assets_fp2, 'bs_current_assets_fp1' => $request->bs_current_assets_fp1,
            'bs_total_liabilities_py3' => $request->bs_total_liabilities_py3, 'bs_total_liabilities_py2' => $request->bs_total_liabilities_py2,
            'bs_total_liabilities_py1' => $request->bs_total_liabilities_py1, 'bs_total_liabilities_ytd' => $request->bs_total_liabilities_ytd,
            'bs_total_liabilities_fp5' => $request->bs_total_liabilities_fp5, 'bs_total_liabilities_fp4' => $request->bs_total_liabilities_fp4,
            'bs_total_liabilities_fp3' => $request->bs_total_liabilities_fp3, 'bs_total_liabilities_fp2' => $request->bs_total_liabilities_fp2,
            'bs_total_liabilities_fp1' => $request->bs_total_liabilities_fp1, 'bs_longterm_liabilities_py3' => $request->bs_longterm_liabilities_py3,
            'bs_longterm_liabilities_py2' => $request->bs_longterm_liabilities_py2, 'bs_longterm_liabilities_py1' => $request->bs_longterm_liabilities_py1,
            'bs_longterm_liabilities_ytd' => $request->bs_longterm_liabilities_ytd, 'bs_longterm_liabilities_fp5' => $request->bs_longterm_liabilities_fp5,
            'bs_longterm_liabilities_fp4' => $request->bs_longterm_liabilities_fp4, 'bs_longterm_liabilities_fp3' => $request->bs_longterm_liabilities_fp3,
            'bs_longterm_liabilities_fp2' => $request->bs_longterm_liabilities_fp2, 'bs_longterm_liabilities_fp1' => $request->bs_longterm_liabilities_fp1,
            'bs_current_liabilities_py3' => $request->bs_current_liabilities_py3, 'bs_current_liabilities_py2' => $request->bs_current_liabilities_py2,
            'bs_current_liabilities_py1' => $request->bs_current_liabilities_py1, 'bs_current_liabilities_ytd' => $request->bs_current_liabilities_ytd,
            'bs_current_liabilities_fp5' => $request->bs_current_liabilities_fp5, 'bs_current_liabilities_fp4' => $request->bs_current_liabilities_fp4,
            'bs_current_liabilities_fp3' => $request->bs_current_liabilities_fp3, 'bs_current_liabilities_fp2' => $request->bs_current_liabilities_fp2,
            'bs_current_liabilities_fp1' => $request->bs_current_liabilities_fp1, 'bs_equity_py3' => $request->bs_equity_py3,
            'bs_equity_py2' => $request->bs_equity_py2, 'bs_equity_py1' => $request->bs_equity_py1, 'bs_equity_ytd' =>
            $request->bs_equity_ytd, 'bs_equity_fp5' => $request->bs_equity_fp5, 'bs_equity_fp4' => $request->bs_equity_fp4,
            'bs_equity_fp3' => $request->bs_equity_fp3, 'bs_equity_fp2' => $request->bs_equity_fp2, 'bs_equity_fp1' => $request->bs_equity_fp1,
            'bs_shareholder_loans_py3' => $request->bs_shareholder_loans_py3, 'bs_shareholder_loans_py2' => $request->bs_shareholder_loans_py2,
            'bs_shareholder_loans_py1' => $request->bs_shareholder_loans_py1, 'bs_shareholder_loans_ytd' => $request->bs_shareholder_loans_ytd,
            'bs_shareholder_loans_fp5' => $request->bs_shareholder_loans_fp5, 'bs_shareholder_loans_fp4' => $request->bs_shareholder_loans_fp4,
            'bs_shareholder_loans_fp3' => $request->bs_shareholder_loans_fp3, 'bs_shareholder_loans_fp2' => $request->bs_shareholder_loans_fp2,
            'bs_shareholder_loans_fp1' => $request->bs_shareholder_loans_fp1, 'bs_capital_expenditure_py3' => $request->bs_capital_expenditure_py3,
            'bs_capital_expenditure_py2' => $request->bs_capital_expenditure_py2, 'bs_capital_expenditure_py1' => $request->bs_capital_expenditure_py1,
            'bs_capital_expenditure_ytd' => $request->bs_capital_expenditure_ytd, 'bs_capital_expenditure_fp5' => $request->bs_capital_expenditure_fp5,
            'bs_capital_expenditure_fp4' => $request->bs_capital_expenditure_fp4, 'bs_capital_expenditure_fp3' => $request->bs_capital_expenditure_fp3,
            'bs_capital_expenditure_fp2' => $request->bs_capital_expenditure_fp2, 'bs_capital_expenditure_fp1' => $request->bs_capital_expenditure_fp1
        ]);
    }

    /**
     *
     * insert tier1 keyratios calculations into db
     *
     * @param Request request
     **/
    public function submit_tier1_keyratios($request)
    {
        $revenue_growth = $this->revenue_growth($request);
        $pbt_margin = $this->pbt_margin($request);
        $ebitda = $this->ebitda($request);
        $tnav = $this->tnav($request);
        $current_ratio = $this->current_ratio($request);
        $debt_ratio = $this->debt_ratio($request);

        //GP Margin >= 50%
        $gp_margin = $this->gp_margin($request);
        $mean_gp_margin = (array_sum($gp_margin) / 4);
        $final_gp_margin = number_format($mean_gp_margin, 1);

        //EBITDA Margin >= 15%
        $ebitda_margin = $this->ebitda_margin($request);
        $mean_ebitda_margin = (array_sum($ebitda_margin) / 4);
        $final_ebitda_margin = number_format($mean_ebitda_margin, 1);

        //PAT Margin >= 10%
        $pat_margin = $this->pat_margin($request);
        $mean_pat_margin = (array_sum($pat_margin) / 4);
        $final_pat_margin = number_format($mean_pat_margin, 1);


        DB::table('key_ratios')->insert([
            'tier' => 1,
            'py3_revenue_growth' =>  "", 'py2_revenue_growth' =>  $revenue_growth[0], 'py1_revenue_growth' =>  $revenue_growth[1],
            'ytd_revenue_growth' =>  $revenue_growth[2], 'fp1_revenue_growth' =>  $revenue_growth[3], 'fp2_revenue_growth' =>  $revenue_growth[4],
            'fp3_revenue_growth' =>  $revenue_growth[5], 'fp4_revenue_growth' =>  $revenue_growth[6], 'fp5_revenue_growth' =>  $revenue_growth[7],

            'py3_gp_margin' =>  $gp_margin[0], 'py2_gp_margin' =>  $gp_margin[1], 'py1_gp_margin' =>  $gp_margin[2], 'ytd_gp_margin' =>  $gp_margin[3],
            'fp1_gp_margin' =>  $gp_margin[4], 'fp2_gp_margin' =>  $gp_margin[5], 'fp3_gp_margin' =>  $gp_margin[6], 'fp4_gp_margin' =>  $gp_margin[7],
            'fp5_gp_margin' =>  $gp_margin[8],

            'py3_pbt_margin' =>  $pbt_margin[0], 'py2_pbt_margin' =>  $pbt_margin[1], 'py1_pbt_margin' =>  $pbt_margin[2],
            'ytd_pbt_margin' =>  $pbt_margin[3], 'fp1_pbt_margin' =>  $pbt_margin[4], 'fp2_pbt_margin' =>  $pbt_margin[5],
            'fp3_pbt_margin' =>  $pbt_margin[6], 'fp4_pbt_margin' =>  $pbt_margin[7], 'fp5_pbt_margin' =>  $pbt_margin[8],

            'py3_ebitda' =>  $ebitda[0], 'py2_ebitda' =>  $ebitda[1], 'py1_ebitda' =>  $ebitda[2], 'ytd_ebitda' =>  $ebitda[3],
            'fp1_ebitda' =>  $ebitda[4], 'fp2_ebitda' =>  $ebitda[5], 'fp3_ebitda' =>  $ebitda[6], 'fp4_ebitda' =>  $ebitda[7],
            'fp5_ebitda' =>  $ebitda[8],

            'py3_ebitda_margin' =>  $ebitda_margin[0], 'py2_ebitda_margin' =>  $ebitda_margin[1], 'py1_ebitda_margin' =>  $ebitda_margin[2],
            'ytd_ebitda_margin' =>  $ebitda_margin[3], 'fp1_ebitda_margin' =>  $ebitda_margin[4], 'fp2_ebitda_margin' =>  $ebitda_margin[5],
            'fp3_ebitda_margin' =>  $ebitda_margin[6], 'fp4_ebitda_margin' =>  $ebitda_margin[7], 'fp5_ebitda_margin' =>  $ebitda_margin[8],

            'py3_pat_margin' =>  $pat_margin[0], 'py2_pat_margin' =>  $pat_margin[1], 'py1_pat_margin' =>  $pat_margin[2],
            'ytd_pat_margin' =>  $pat_margin[3], 'fp1_pat_margin' =>  $pat_margin[4], 'fp2_pat_margin' =>  $pat_margin[5],
            'fp3_pat_margin' =>  $pat_margin[6], 'fp4_pat_margin' =>  $pat_margin[7], 'fp5_pat_margin' =>  $pat_margin[8],

            'py3_tnav' =>  $tnav[0], 'py2_tnav' =>  $tnav[1], 'py1_tnav' =>  $tnav[2], 'ytd_tnav' =>  $tnav[3], 'fp1_tnav' =>  $tnav[4],
            'fp2_tnav' =>  $tnav[5], 'fp3_tnav' =>  $tnav[6], 'fp4_tnav' =>  $tnav[7], 'fp5_tnav' =>  $tnav[8],

            'py3_current_ratio' =>  $current_ratio[0], 'py2_current_ratio' =>  $current_ratio[1], 'py1_current_ratio' =>  $current_ratio[2],
            'ytd_current_ratio' =>  $current_ratio[3], 'fp1_current_ratio' =>  $current_ratio[4], 'fp2_current_ratio' =>  $current_ratio[5],
            'fp3_current_ratio' =>  $current_ratio[6], 'fp4_current_ratio' =>  $current_ratio[7], 'fp5_current_ratio' =>  $current_ratio[8],

            'py3_debt_ratio' =>  $debt_ratio[0], 'py2_debt_ratio' =>  $debt_ratio[1], 'py1_debt_ratio' =>  $debt_ratio[2],
            'ytd_debt_ratio' =>  $debt_ratio[3], 'fp1_debt_ratio' =>  $debt_ratio[4], 'fp2_debt_ratio' =>  $debt_ratio[5],
            'fp3_debt_ratio' =>  $debt_ratio[6], 'fp4_debt_ratio' =>  $debt_ratio[7], 'fp5_debt_ratio' =>  $debt_ratio[8],
        ]);
    }

    /**
     * insert tier2 calculations into db
     *
     * @param Request $request
     **/
    public function submit_tier2_keyratios($request)
    {
        $revenue_growth = $this->revenue_growth($request);
        $pbt_margin = $this->pbt_margin($request);
        $ebitda = $this->ebitda($request);
        $tnav = $this->tnav($request);
        $current_ratio = $this->current_ratio($request);
        $debt_ratio = $this->debt_ratio($request);

        //GP Margin >= 50%
        $gp_margin = $this->gp_margin($request);
        $mean_gp_margin = (array_sum($gp_margin) / 4);
        $final_gp_margin = number_format($mean_gp_margin, 1);

        //EBITDA Margin >= 15%
        $ebitda_margin = $this->ebitda_margin($request);
        $mean_ebitda_margin = (array_sum($ebitda_margin) / 4);
        $final_ebitda_margin = number_format($mean_ebitda_margin, 1);

        //PAT Margin >= 10%
        $pat_margin = $this->pat_margin($request);
        $mean_pat_margin = (array_sum($pat_margin) / 4);
        $final_pat_margin = number_format($mean_pat_margin, 1);


        DB::table('key_ratios')->insert([
            'tier' => 2,
            'py3_revenue_growth' =>  "", 'py2_revenue_growth' =>  $revenue_growth[0], 'py1_revenue_growth' =>  $revenue_growth[1],
            'ytd_revenue_growth' =>  $revenue_growth[2], 'fp1_revenue_growth' =>  $revenue_growth[3], 'fp2_revenue_growth' =>  $revenue_growth[4],
            'fp3_revenue_growth' =>  $revenue_growth[5], 'fp4_revenue_growth' =>  $revenue_growth[6], 'fp5_revenue_growth' =>  $revenue_growth[7],

            'py3_gp_margin' =>  $gp_margin[0], 'py2_gp_margin' =>  $gp_margin[1], 'py1_gp_margin' =>  $gp_margin[2], 'ytd_gp_margin' =>  $gp_margin[3],
            'fp1_gp_margin' =>  $gp_margin[4], 'fp2_gp_margin' =>  $gp_margin[5], 'fp3_gp_margin' =>  $gp_margin[6], 'fp4_gp_margin' =>  $gp_margin[7],
            'fp5_gp_margin' =>  $gp_margin[8],

            'py3_pbt_margin' =>  $pbt_margin[0], 'py2_pbt_margin' =>  $pbt_margin[1], 'py1_pbt_margin' =>  $pbt_margin[2],
            'ytd_pbt_margin' =>  $pbt_margin[3], 'fp1_pbt_margin' =>  $pbt_margin[4], 'fp2_pbt_margin' =>  $pbt_margin[5],
            'fp3_pbt_margin' =>  $pbt_margin[6], 'fp4_pbt_margin' =>  $pbt_margin[7], 'fp5_pbt_margin' =>  $pbt_margin[8],

            'py3_ebitda' =>  $ebitda[0], 'py2_ebitda' =>  $ebitda[1], 'py1_ebitda' =>  $ebitda[2], 'ytd_ebitda' =>  $ebitda[3],
            'fp1_ebitda' =>  $ebitda[4], 'fp2_ebitda' =>  $ebitda[5], 'fp3_ebitda' =>  $ebitda[6], 'fp4_ebitda' =>  $ebitda[7],
            'fp5_ebitda' =>  $ebitda[8],

            'py3_ebitda_margin' =>  $ebitda_margin[0], 'py2_ebitda_margin' =>  $ebitda_margin[1], 'py1_ebitda_margin' =>  $ebitda_margin[2],
            'ytd_ebitda_margin' =>  $ebitda_margin[3], 'fp1_ebitda_margin' =>  $ebitda_margin[4], 'fp2_ebitda_margin' =>  $ebitda_margin[5],
            'fp3_ebitda_margin' =>  $ebitda_margin[6], 'fp4_ebitda_margin' =>  $ebitda_margin[7], 'fp5_ebitda_margin' =>  $ebitda_margin[8],

            'py3_pat_margin' =>  $pat_margin[0], 'py2_pat_margin' =>  $pat_margin[1], 'py1_pat_margin' =>  $pat_margin[2],
            'ytd_pat_margin' =>  $pat_margin[3], 'fp1_pat_margin' =>  $pat_margin[4], 'fp2_pat_margin' =>  $pat_margin[5],
            'fp3_pat_margin' =>  $pat_margin[6], 'fp4_pat_margin' =>  $pat_margin[7], 'fp5_pat_margin' =>  $pat_margin[8],

            'py3_tnav' =>  $tnav[0], 'py2_tnav' =>  $tnav[1], 'py1_tnav' =>  $tnav[2], 'ytd_tnav' =>  $tnav[3], 'fp1_tnav' =>  $tnav[4],
            'fp2_tnav' =>  $tnav[5], 'fp3_tnav' =>  $tnav[6], 'fp4_tnav' =>  $tnav[7], 'fp5_tnav' =>  $tnav[8],

            'py3_current_ratio' =>  $current_ratio[0], 'py2_current_ratio' =>  $current_ratio[1], 'py1_current_ratio' =>  $current_ratio[2],
            'ytd_current_ratio' =>  $current_ratio[3], 'fp1_current_ratio' =>  $current_ratio[4], 'fp2_current_ratio' =>  $current_ratio[5],
            'fp3_current_ratio' =>  $current_ratio[6], 'fp4_current_ratio' =>  $current_ratio[7], 'fp5_current_ratio' =>  $current_ratio[8],

            'py3_debt_ratio' =>  $debt_ratio[0], 'py2_debt_ratio' =>  $debt_ratio[1], 'py1_debt_ratio' =>  $debt_ratio[2],
            'ytd_debt_ratio' =>  $debt_ratio[3], 'fp1_debt_ratio' =>  $debt_ratio[4], 'fp2_debt_ratio' =>  $debt_ratio[5],
            'fp3_debt_ratio' =>  $debt_ratio[6], 'fp4_debt_ratio' =>  $debt_ratio[7], 'fp5_debt_ratio' =>  $debt_ratio[8],
        ]);
    }


    /********************************* KEY RATIOS CALCULATIONS ******************************************************************* */
    /**
     *
     * Calculate revenue growth
     *
     * ((current revenue / prior year revenue) - 1) x 100
     *
     * @param Request $request
     **/
    public function revenue_growth($request)
    {
        return array(
            $request->is_revenue_py3 == 0 ? 0 : number_format(((((float)$request->is_revenue_py2 / $request->is_revenue_py3)  - 1) * 100), 1),
            $request->is_revenue_py2 == 0 ? 0 : number_format(((((float)$request->is_revenue_py1 / $request->is_revenue_py2)  - 1) * 100), 1),
            $request->is_revenue_py1 == 0 ? 0 : number_format(((((float)$request->is_revenue_ytd / $request->is_revenue_py1)  - 1) * 100), 1),
            $request->is_revenue_ytd == 0 ? 0 : number_format(((((float)$request->is_revenue_fp1 / $request->is_revenue_ytd)  - 1) * 100), 1),
            $request->is_revenue_fp1 == 0 ? 0 : number_format(((((float)$request->is_revenue_fp2 / $request->is_revenue_fp1)  - 1) * 100), 1),
            $request->is_revenue_fp2 == 0 ? 0 : number_format(((((float)$request->is_revenue_fp3 / $request->is_revenue_fp2)  - 1) * 100), 1),
            $request->is_revenue_fp3 == 0 ? 0 : number_format(((((float)$request->is_revenue_fp4 / $request->is_revenue_fp3)  - 1) * 100), 1),
            $request->is_revenue_fp4 == 0 ? 0 : number_format(((((float)$request->is_revenue_fp5 / $request->is_revenue_fp4)  - 1) * 100), 1),
        );
    }

    /**
     *
     * Calculate gross profit margin
     *
     * gross profit / revenue
     *
     * @param Request $request
     **/
    public function gp_margin($request)
    {
        return array(
            $request->is_revenue_py3 == 0 ? 0 : number_format(((float)$request->is_gross_profit_py3 / (float)$request->is_revenue_py3 * 100), 1),
            $request->is_revenue_py2 == 0 ? 0 : number_format(((float)$request->is_gross_profit_py2 / (float)$request->is_revenue_py2 * 100), 1),
            $request->is_revenue_py1 == 0 ? 0 : number_format(((float)$request->is_gross_profit_py1 / (float)$request->is_revenue_py1 * 100), 1),
            $request->is_revenue_ytd == 0 ? 0 : number_format(((float)$request->is_gross_profit_ytd / (float)$request->is_revenue_ytd * 100), 1),
            $request->is_revenue_fp1 == 0 ? 0 : number_format(((float)$request->is_gross_profit_fp1 / (float)$request->is_revenue_fp1 * 100), 1),
            $request->is_revenue_fp2 == 0 ? 0 : number_format(((float)$request->is_gross_profit_fp2 / (float)$request->is_revenue_fp2 * 100), 1),
            $request->is_revenue_fp3 == 0 ? 0 : number_format(((float)$request->is_gross_profit_fp3 / (float)$request->is_revenue_fp3 * 100), 1),
            $request->is_revenue_fp4 == 0 ? 0 : number_format(((float)$request->is_gross_profit_fp4 / (float)$request->is_revenue_fp4 * 100), 1),
            $request->is_revenue_fp5 == 0 ? 0 : number_format(((float)$request->is_gross_profit_fp5 / (float)$request->is_revenue_fp5 * 100), 1),
        );
    }

    /**
     *
     * Calculate profit before tax margin
     *
     * profit before tax margin (PBT / Revenue)
     *
     * @param Request $request
     **/
    public function pbt_margin($request)
    {
        return array(
            $request->is_revenue_py3 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_py3 / (float)$request->is_revenue_py3 * 100), 1),
            $request->is_revenue_py2 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_py2 / (float)$request->is_revenue_py2 * 100), 1),
            $request->is_revenue_py1 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_py1 / (float)$request->is_revenue_py1 * 100), 1),
            $request->is_revenue_ytd == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_ytd / (float)$request->is_revenue_ytd * 100), 1),
            $request->is_revenue_fp1 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_fp1 / (float)$request->is_revenue_fp1 * 100), 1),
            $request->is_revenue_fp2 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_fp2 / (float)$request->is_revenue_fp2 * 100), 1),
            $request->is_revenue_fp3 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_fp3 / (float)$request->is_revenue_fp3 * 100), 1),
            $request->is_revenue_fp4 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_fp4 / (float)$request->is_revenue_fp4 * 100), 1),
            $request->is_revenue_fp5 == 0 ? 0 : number_format(((float)$request->is_profit_before_tax_fp5 / (float)$request->is_revenue_fp5 * 100), 1),
        );
    }

    /**
     *
     * Calculate ebitda
     *
     * profit before tax + interest expense + depreciation + amortization
     *
     * @param Request $request
     **/
    public function ebitda($request)
    {
        return array(
            number_format(((float)$request->is_profit_before_tax_py3 + (float)$request->is_interest_expense_py3 + (float)$request->is_depreciation_py3), 1),
            number_format(((float)$request->is_profit_before_tax_py2 + (float)$request->is_interest_expense_py2 + (float)$request->is_depreciation_py2), 1),
            number_format(((float)$request->is_profit_before_tax_py1 + (float)$request->is_interest_expense_py1 + (float)$request->is_depreciation_py1), 1),
            number_format(((float)$request->is_profit_before_tax_ytd + (float)$request->is_interest_expense_ytd + (float)$request->is_depreciation_ytd), 1),
            number_format(((float)$request->is_profit_before_tax_fp1 + (float)$request->is_interest_expense_fp1 + (float)$request->is_depreciation_fp1), 1),
            number_format(((float)$request->is_profit_before_tax_fp2 + (float)$request->is_interest_expense_fp2 + (float)$request->is_depreciation_fp2), 1),
            number_format(((float)$request->is_profit_before_tax_fp3 + (float)$request->is_interest_expense_fp3 + (float)$request->is_depreciation_fp3), 1),
            number_format(((float)$request->is_profit_before_tax_fp4 + (float)$request->is_interest_expense_fp4 + (float)$request->is_depreciation_fp4), 1),
            number_format(((float)$request->is_profit_before_tax_fp5 + (float)$request->is_interest_expense_fp5 + (float)$request->is_depreciation_fp5), 1),
        );
    }

    /**
     *
     * Calculate ebitda margin
     *
     * EBITDA / Revenue
     *
     * @param Request $request
     **/
    public function ebitda_margin($request)
    {
        return array(
            $request->is_revenue_py3 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_py3 + (float)$request->is_interest_expense_py3 +
                (float)$request->is_depreciation_py3) / (float)$request->is_revenue_py3 * 100), 1),
            $request->is_revenue_py2 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_py2 + (float)$request->is_interest_expense_py2 +
                (float)$request->is_depreciation_py2) / (float)$request->is_revenue_py2 * 100), 1),
            $request->is_revenue_py1 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_py1 + (float)$request->is_interest_expense_py1 +
                (float)$request->is_depreciation_py1) / (float)$request->is_revenue_py1 * 100), 1),
            $request->is_revenue_ytd == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_ytd + (float)$request->is_interest_expense_ytd +
                (float)$request->is_depreciation_ytd) / (float)$request->is_revenue_ytd * 100), 1),
            $request->is_revenue_fp1 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_fp1 + (float)$request->is_interest_expense_fp1 +
                (float)$request->is_depreciation_fp1) / (float)$request->is_revenue_fp1 * 100), 1),
            $request->is_revenue_fp2 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_fp2 + (float)$request->is_interest_expense_fp2 +
                (float)$request->is_depreciation_fp2) / (float)$request->is_revenue_fp2 * 100), 1),
            $request->is_revenue_fp3 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_fp3 + (float)$request->is_interest_expense_fp3 +
                (float)$request->is_depreciation_fp3) / (float)$request->is_revenue_fp3 * 100), 1),
            $request->is_revenue_fp4 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_fp4 + (float)$request->is_interest_expense_fp4 +
                (float)$request->is_depreciation_fp4) / (float)$request->is_revenue_fp4 * 100), 1),
            $request->is_revenue_fp5 == 0 ? 0 : number_format((((float)$request->is_profit_before_tax_fp5 + (float)$request->is_interest_expense_fp5 +
                (float)$request->is_depreciation_fp5) / (float)$request->is_revenue_fp5 * 100), 1),
        );
    }

    /**
     *
     * Calculate profit after tax margin
     *
     * profit after tax margin (PAT / Revenue)
     *
     * @param Request $request
     **/
    public function pat_margin($request)
    {
        return array(
            $request->is_revenue_py3 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_py3 / (float)$request->is_revenue_py3 * 100), 1),
            $request->is_revenue_py2 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_py2 / (float)$request->is_revenue_py2 * 100), 1),
            $request->is_revenue_py1 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_py1 / (float)$request->is_revenue_py1 * 100), 1),
            $request->is_revenue_ytd == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_ytd / (float)$request->is_revenue_ytd * 100), 1),
            $request->is_revenue_fp1 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_fp1 / (float)$request->is_revenue_fp1 * 100), 1),
            $request->is_revenue_fp2 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_fp2 / (float)$request->is_revenue_fp2 * 100), 1),
            $request->is_revenue_fp3 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_fp3 / (float)$request->is_revenue_fp3 * 100), 1),
            $request->is_revenue_fp4 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_fp4 / (float)$request->is_revenue_fp4 * 100), 1),
            $request->is_revenue_fp5 == 0 ? 0 : number_format(((float)$request->is_profit_after_tax_fp5 / (float)$request->is_revenue_fp5 * 100), 1),
        );
    }

    /**
     *
     * Calculate TNAV
     *
     * Total assets - intangibles - total debt/liabilities
     *
     * @param Request $request
     **/
    public function tnav($request)
    {
        return array(
            number_format((float)$request->bs_total_assets_py3 - (float)$request->bs_total_liabilities_py3, 1),
            number_format((float)$request->bs_total_assets_py2 - (float)$request->bs_total_liabilities_py2, 1),
            number_format((float)$request->bs_total_assets_py1 - (float)$request->bs_total_liabilities_py1, 1),
            number_format((float)$request->bs_total_assets_ytd - (float)$request->bs_total_liabilities_ytd, 1),
            number_format((float)$request->bs_total_assets_fp1 - (float)$request->bs_total_liabilities_fp1, 1),
            number_format((float)$request->bs_total_assets_fp2 - (float)$request->bs_total_liabilities_fp2, 1),
            number_format((float)$request->bs_total_assets_fp3 - (float)$request->bs_total_liabilities_fp3, 1),
            number_format((float)$request->bs_total_assets_fp4 - (float)$request->bs_total_liabilities_fp4, 1),
            number_format((float)$request->bs_total_assets_fp5 - (float)$request->bs_total_liabilities_fp5, 1),
        );
    }

    /**
     *
     * Calculate current ratio
     *
     * current assets / current liabilities
     *
     * @param Request $request
     **/
    public function current_ratio($request)
    {
        return array(
            $request->bs_current_liabilities_py3 == 0 ? 0 : number_format(((float)$request->bs_current_assets_py3 / (float)$request->bs_current_liabilities_py3), 1),
            $request->bs_current_liabilities_py2 == 0 ? 0 : number_format(((float)$request->bs_current_assets_py2 / (float)$request->bs_current_liabilities_py2), 1),
            $request->bs_current_liabilities_py1 == 0 ? 0 : number_format(((float)$request->bs_current_assets_py1 / (float)$request->bs_current_liabilities_py1), 1),
            $request->bs_current_liabilities_ytd == 0 ? 0 : number_format(((float)$request->bs_current_assets_ytd / (float)$request->bs_current_liabilities_ytd), 1),
            $request->bs_current_liabilities_fp1 == 0 ? 0 : number_format(((float)$request->bs_current_assets_fp1 / (float)$request->bs_current_liabilities_fp1), 1),
            $request->bs_current_liabilities_fp2 == 0 ? 0 : number_format(((float)$request->bs_current_assets_fp2 / (float)$request->bs_current_liabilities_fp2), 1),
            $request->bs_current_liabilities_fp3 == 0 ? 0 : number_format(((float)$request->bs_current_assets_fp3 / (float)$request->bs_current_liabilities_fp3), 1),
            $request->bs_current_liabilities_fp4 == 0 ? 0 : number_format(((float)$request->bs_current_assets_fp4 / (float)$request->bs_current_liabilities_fp4), 1),
            $request->bs_current_liabilities_fp5 == 0 ? 0 : number_format(((float)$request->bs_current_assets_fp5 / (float)$request->bs_current_liabilities_fp5), 1),
        );
    }

    /**
     *
     * Calculate debt ratio
     *
     * Total Debt/liabilities / Total Assets
     *
     * @param Request $request
     **/
    public function debt_ratio($request)
    {
        return array(
            $request->bs_total_assets_py3 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_py3 / (float)$request->bs_total_assets_py3), 1),
            $request->bs_total_assets_py2 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_py2 / (float)$request->bs_total_assets_py2), 1),
            $request->bs_total_assets_py1 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_py1 / (float)$request->bs_total_assets_py1), 1),
            $request->bs_total_assets_ytd == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_ytd / (float)$request->bs_total_assets_ytd), 1),
            $request->bs_total_assets_fp1 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_fp1 / (float)$request->bs_total_assets_fp1), 1),
            $request->bs_total_assets_fp2 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_fp2 / (float)$request->bs_total_assets_fp2), 1),
            $request->bs_total_assets_fp3 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_fp3 / (float)$request->bs_total_assets_fp3), 1),
            $request->bs_total_assets_fp4 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_fp4 / (float)$request->bs_total_assets_fp4), 1),
            $request->bs_total_assets_fp5 == 0 ? 0 : number_format(((float)$request->bs_total_liabilities_fp5 / (float)$request->bs_total_assets_fp5), 1),
        );
    }

    /**
     *
     * get applicant forms view
     *
     * @param Request $request
     **/
    public function get_applicant(Request $request)
    {
        $form_applicants = array($request->all());

        $status = ['0', '1'];
        $races = ['white', 'african-black', 'indian', 'colored'];

        if (isset($_POST['submit'])) {

            if (session()->has('session_applicants')) {
                $session_applicants = session()->get('session_applicants');
                session()->flash('info_message', 'ATTENTION! You have an Incomplete registration you can continue filling it below');
                return view('forms.applicant')
                    ->with('races', $races)
                    ->with('status', $status)
                    ->with('session_applicants', $session_applicants)
                    ->with('form_applicants', $form_applicants);
            }
        }

        if (isset($_POST['save'])) {
            session()->put('session_applicants', $form_applicants);
            $request->session()->flash('success_message', 'Registration saved for Later, You can complete registration later.');
            return back();
        }

        $session_applicants = session()->get('session_applicants');
        return view('forms.applicant')
            ->with('races', $races)
            ->with('status', $status)
            ->with('session_applicants', $session_applicants)
            ->with('form_applicants', $form_applicants);
    }

    /**
     * get compliance form view
     *
     * @param Request $request
     * @return applicants
     **/
    public function applicant_registration(Request $request)
    {
        $form_applicants = array($request->all());
        $races = ['white', 'african-black', 'indian', 'colored'];
        $status = ['0', '1'];

        $validator = Validator::make($request->all(), [
            'firstname' => ['required', 'string', 'max:255'], 'lastname' => ['required', 'string', 'max:255'],
            'race' => ['required', 'string', 'max:255'], 'nationality' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'], 'phone' => ['required', 'string', 'max:255'],
            'business_status' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return view('forms.applicant')
                ->with('races', $races)
                ->with('status', $status)
                ->withErrors($validator);
        }

        if (isset($_POST['submit'])) {

            if (session()->has('session_compliants')) {
                $session_compliants = session()->get('session_compliants');
                session()->flash('info_message', 'ATTENTION! You have an Incomplete registration you can continue filling it below');
                return view('forms.compliance')
                    ->with('races', $races)
                    ->with('status', $status)
                    ->with('session_compliants', $session_compliants)
                    ->with('form_applicants', $form_applicants);
            }

            $session_compliants = session()->get('session_compliants');
            return view('forms.compliance')
                ->with('session_compliants', $session_compliants)
                ->with('form_applicants', $form_applicants);
        }

        if (isset($_POST['save'])) {
            session()->put('session_applicants', $form_applicants);
            $request->session()->flash('success_message', 'Registration saved for Later, You can complete registration later.');
            return back();
        }
    }


    /**
     * get background form view
     *
     * @param Request $request
     * @return applicants
     * @return compliants
     **/
    public function compliance_registration(Request $request)
    {
        $form_applicants = array($request->all());
        $form_compliants = array($request->all());
        $form_backgrounds = array($request->all());

        $validate = Validator::make($request->all(), [
            'company_name'  => ['required', 'string', 'max:255'], 'registration_no'  => ['required', 'string', 'max:255'],
            'tax_registration_no'  => ['required', 'string', 'max:255'], 'vat_registration_no'  => ['required', 'string', 'max:255'],
            'contact_person'  => ['required', 'string', 'max:255'], 'contact_person_email'  => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'], 'contact_person_mobile'  => ['required', 'string', 'max:255'],
        ]);

        if ($validate->fails()) {
            return view('forms.compliance')->with('form_applicants', $form_applicants)
                ->withErrors($validate);
        }

        if (isset($_POST['submit'])) {

            if (session()->has('session_backgrounds')) {
                $session_backgrounds = session()->get('session_backgrounds');
                $request->session()->flash('info_message', 'ATTENTION! You have an incomplete registration you can continue filling it below');
                return view('forms.background')
                    ->with('form_applicants', $form_applicants)
                    ->with('form_compliants', $form_compliants)
                    ->with('form_backgrounds', $form_backgrounds)
                    ->with('session_backgrounds', $session_backgrounds);
            }

            $session_backgrounds = session()->get('session_backgrounds');
            return view('forms.background')
                ->with('form_applicants', $form_applicants)
                ->with('form_compliants', $form_compliants)
                ->with('form_backgrounds', $form_backgrounds)
                ->with('session_backgrounds', $session_backgrounds);
        }


        if (isset($_POST['save'])) {
            session()->put('session_applicants', $form_applicants);
            session()->put('session_compliants', $form_compliants);
            $request->session()->flash('success_message', 'Registration saved for Later, You can complete registration later.');
            return redirect('/');
        }
    }

    /**
     *
     * get financia form view
     *
     * @param Request $request
     * @return applicants
     * @return backgrounds
     * @return compliants
     **/
    public function background_registration(Request $request)
    {
        $form_compliants = array($request->all());
        $form_applicants = array($request->all());
        $form_backgrounds = array($request->all());

        $validation = Validator::make($request->all(), [
            'business_location_province' => ['required', 'string', 'max:255'], 'business_nearest_city_location' => ['required', 'string', 'max:255'],
            'project_actual_location_area' => ['required', 'string', 'max:255'], 'business_financial_year_end' => ['required', 'string', 'max:255'], 'business_accountant_name' => ['required', 'string', 'max:255']
        ]);

        if ($validation->fails()) {
            return view('forms.background')->with('form_applicants', $form_applicants)
                ->with('form_compliants', $form_compliants)
                ->withErrors($validation);
        }

        if (isset($_POST['submit'])) {

            if (session()->has('session_financials')) {
                $session_financials = session()->get('session_financials');
                $request->session()->flash('info_message', 'ATTENTION! You have an incomplete registration you can continue filling it below');
                return view('forms.financial')
                    ->with('form_applicants', $form_applicants)
                    ->with('form_compliants', $form_compliants)
                    ->with('form_backgrounds', $form_backgrounds)
                    ->with('session_financials', $session_financials);
            }

            $session_financials = session()->get('session_financials');
            return view('forms.financial')
                ->with('form_applicants', $form_applicants)
                ->with('form_compliants', $form_compliants)
                ->with('form_backgrounds', $form_backgrounds)
                ->with('session_financials', $session_financials);
        }

        if (isset($_POST['save'])) {
            session()->put('session_applicants', $form_applicants);
            session()->put('session_compliants', $form_compliants);
            session()->put('session_backgrounds', $form_backgrounds);
            $request->session()->flash('success_message', 'Registration saved for Later, You can complete registration later.');
            return redirect('/');
        }
    }

    /**
     * calculate gp,ebitda,pat margin
     * submit financial registration as per margin calculations
     * send email notification after a successful registration
     *
     * profit before tax margin (PBT / Revenue)
     *
     * @param Request $request
     **/
    public function financial_registration(Request $request)
    {
        $form_applicants = array($request->all());
        $form_compliants = array($request->all());
        $form_backgrounds = array($request->all());
        $form_financials = array($request->all());

        $valid = Validator::make($request->all(), [
            'is_revenue_py3' => ['required', 'max:10'], 'is_revenue_py2' => ['required', 'max:10'],
            'is_revenue_py1' => ['required', 'max:10'], 'is_revenue_ytd' => ['required', 'max:10'],
            'is_revenue_fp5' => ['required', 'max:10'], 'is_revenue_fp4' => ['required', 'max:10'],
            'is_revenue_fp3' => ['required', 'max:10'], 'is_revenue_fp2' => ['required', 'max:10'],
            'is_revenue_fp1' => ['required', 'max:10'], 'is_cost_of_sales_py3' => ['required', 'max:10'],
            'is_cost_of_sales_py2' => ['required', 'max:10'],
            'is_cost_of_sales_py1' => ['required', 'max:10'], 'is_cost_of_sales_ytd' => ['required', 'max:10'],
            'is_cost_of_sales_fp5' => ['required', 'max:10'], 'is_cost_of_sales_fp4' => ['required', 'max:10'],
            'is_cost_of_sales_fp3' => ['required', 'max:10'], 'is_cost_of_sales_fp2' => ['required', 'max:10'],
            'is_cost_of_sales_fp1' => ['required', 'max:10'], 'is_gross_profit_py3' => ['required', 'max:10'],
            'is_gross_profit_py2' => ['required', 'max:10'], 'is_gross_profit_py1' => ['required', 'max:10'],
            'is_gross_profit_ytd' => ['required', 'max:10'], 'is_gross_profit_fp5' => ['required', 'max:10'],
            'is_gross_profit_fp4' => ['required', 'max:10'], 'is_gross_profit_fp3' => ['required', 'max:10'],
            'is_gross_profit_fp2' => ['required', 'max:10'], 'is_gross_profit_fp1' => ['required', 'max:10'],
            'is_operating_expenses_py3' => ['required', 'max:10'], 'is_operating_expenses_py2' => ['required', 'max:10'],
            'is_operating_expenses_py1' => ['required', 'max:10'], 'is_operating_expenses_ytd' => ['required', 'max:10'],
            'is_operating_expenses_fp5' => ['required', 'max:10'], 'is_operating_expenses_fp4' => ['required', 'max:10'],
            'is_operating_expenses_fp3' => ['required', 'max:10'], 'is_operating_expenses_fp2' => ['required', 'max:10'],
            'is_operating_expenses_fp1' => ['required', 'max:10'], 'is_profit_before_tax_py3' => ['required', 'max:10'],
            'is_profit_before_tax_py2' => ['required', 'max:10'], 'is_profit_before_tax_py1' => ['required', 'max:10'],
            'is_profit_before_tax_ytd' => ['required', 'max:10'], 'is_profit_before_tax_fp5' => ['required', 'max:10'],
            'is_profit_before_tax_fp4' => ['required', 'max:10'], 'is_profit_before_tax_fp3' => ['required', 'max:10'],
            'is_profit_before_tax_fp2' => ['required', 'max:10'], 'is_profit_before_tax_fp1' => ['required', 'max:10'],
            'is_tax_py3' => ['required', 'max:10'], 'is_tax_py2' => ['required', 'max:10'], 'is_tax_py1' => ['required', 'max:10'],
            'is_tax_ytd' => ['required', 'max:10'], 'is_tax_fp5' => ['required', 'max:10'], 'is_tax_fp4' => ['required', 'max:10'],
            'is_tax_fp3' => ['required', 'max:10'], 'is_tax_fp2' => ['required', 'max:10'], 'is_tax_fp1' => ['required', 'max:10'],
            'is_profit_after_tax_py3' => ['required', 'max:10'], 'is_profit_after_tax_py2' => ['required', 'max:10'],
            'is_profit_after_tax_py1' => ['required', 'max:10'], 'is_profit_after_tax_ytd' => ['required', 'max:10'],
            'is_profit_after_tax_fp5' => ['required', 'max:10'], 'is_profit_after_tax_fp4' => ['required', 'max:10'],
            'is_profit_after_tax_fp3' => ['required', 'max:10'], 'is_profit_after_tax_fp2' => ['required', 'max:10'],
            'is_profit_after_tax_fp1' => ['required', 'max:10'], 'is_depreciation_py3' => ['required', 'max:10'],
            'is_depreciation_py2' => ['required', 'max:10'], 'is_depreciation_py1' => ['required', 'max:10'],
            'is_depreciation_ytd' => ['required', 'max:10'], 'is_depreciation_fp5' => ['required', 'max:10'],
            'is_depreciation_fp4' => ['required', 'max:10'], 'is_depreciation_fp3' => ['required', 'max:10'],
            'is_depreciation_fp2' => ['required', 'max:10'], 'is_depreciation_fp1' => ['required', 'max:10'],
            'is_interest_expense_py3' => ['required', 'max:10'], 'is_interest_expense_py2' => ['required', 'max:10'],
            'is_interest_expense_py1' => ['required', 'max:10'], 'is_interest_expense_ytd' => ['required', 'max:10'],
            'is_interest_expense_fp5' => ['required', 'max:10'], 'is_interest_expense_fp4' => ['required', 'max:10'],
            'is_interest_expense_fp3' => ['required', 'max:10'], 'is_interest_expense_fp2' => ['required', 'max:10'],
            'is_interest_expense_fp1' => ['required', 'max:10'], 'bs_total_assets_py3' => ['required', 'max:10'],
            'bs_total_assets_py2' => ['required', 'max:10'], 'bs_total_assets_py1' => ['required', 'max:10'],
            'bs_total_assets_ytd' => ['required', 'max:10'], 'bs_total_assets_fp5' => ['required', 'max:10'],
            'bs_total_assets_fp4' => ['required', 'max:10'], 'bs_total_assets_fp3' => ['required', 'max:10'],
            'bs_total_assets_fp2' => ['required', 'max:10'], 'bs_total_assets_fp1' => ['required', 'max:10'],
            'bs_fixed_assets_py3' => ['required', 'max:10'], 'bs_fixed_assets_py2' => ['required', 'max:10'],
            'bs_fixed_assets_py1' => ['required', 'max:10'], 'bs_fixed_assets_ytd' => ['required', 'max:10'],
            'bs_fixed_assets_fp5' => ['required', 'max:10'], 'bs_fixed_assets_fp4' => ['required', 'max:10'],
            'bs_fixed_assets_fp3' => ['required', 'max:10'], 'bs_fixed_assets_fp2' => ['required', 'max:10'],
            'bs_fixed_assets_fp1' => ['required', 'max:10'], 'bs_current_assets_py3' => ['required', 'max:10'],
            'bs_current_assets_py2' => ['required', 'max:10'], 'bs_current_assets_py1' => ['required', 'max:10'],
            'bs_current_assets_ytd' => ['required', 'max:10'], 'bs_current_assets_fp5' => ['required', 'max:10'],
            'bs_current_assets_fp4' => ['required', 'max:10'], 'bs_current_assets_fp3' => ['required', 'max:10'],
            'bs_current_assets_fp2' => ['required', 'max:10'], 'bs_current_assets_fp1' => ['required', 'max:10'],
            'bs_total_liabilities_py3' => ['required', 'max:10'], 'bs_total_liabilities_py2' => ['required', 'max:10'],
            'bs_total_liabilities_py1' => ['required', 'max:10'], 'bs_total_liabilities_ytd' => ['required', 'max:10'],
            'bs_total_liabilities_fp5' => ['required', 'max:10'], 'bs_total_liabilities_fp4' => ['required', 'max:10'],
            'bs_total_liabilities_fp3' => ['required', 'max:10'], 'bs_total_liabilities_fp2' => ['required', 'max:10'],
            'bs_total_liabilities_fp1' => ['required', 'max:10'], 'bs_longterm_liabilities_py3' => ['required', 'max:10'],
            'bs_longterm_liabilities_py2' => ['required', 'max:10'], 'bs_longterm_liabilities_py1' => ['required', 'max:10'],
            'bs_longterm_liabilities_ytd' => ['required', 'max:10'], 'bs_longterm_liabilities_fp5' => ['required', 'max:10'],
            'bs_longterm_liabilities_fp4' => ['required', 'max:10'], 'bs_longterm_liabilities_fp3' => ['required', 'max:10'],
            'bs_longterm_liabilities_fp2' => ['required', 'max:10'], 'bs_longterm_liabilities_fp1' => ['required', 'max:10'],
            'bs_current_liabilities_py3' => ['required', 'max:10'], 'bs_current_liabilities_py2' => ['required', 'max:10'],
            'bs_current_liabilities_py1' => ['required', 'max:10'], 'bs_current_liabilities_ytd' => ['required', 'max:10'],
            'bs_current_liabilities_fp5' => ['required', 'max:10'], 'bs_current_liabilities_fp4' => ['required', 'max:10'],
            'bs_current_liabilities_fp3' => ['required', 'max:10'], 'bs_current_liabilities_fp2' => ['required', 'max:10'],
            'bs_current_liabilities_fp1' => ['required', 'max:10'], 'bs_equity_py3' => ['required', 'max:10'],
            'bs_equity_py2' => ['required', 'max:10'], 'bs_equity_py1' => ['required', 'max:10'], 'bs_equity_ytd' =>
            ['required', 'max:10'], 'bs_equity_fp5' => ['required', 'max:10'], 'bs_equity_fp4' => ['required', 'max:10'],
            'bs_equity_fp3' => ['required', 'max:10'], 'bs_equity_fp2' => ['required', 'max:10'], 'bs_equity_fp1' => ['required', 'max:10'],
            'bs_shareholder_loans_py3' => ['required', 'max:10'], 'bs_shareholder_loans_py2' => ['required', 'max:10'],
            'bs_shareholder_loans_py1' => ['required', 'max:10'], 'bs_shareholder_loans_ytd' => ['required', 'max:10'],
            'bs_shareholder_loans_fp5' => ['required', 'max:10'], 'bs_shareholder_loans_fp4' => ['required', 'max:10'],
            'bs_shareholder_loans_fp3' => ['required', 'max:10'], 'bs_shareholder_loans_fp2' => ['required', 'max:10'],
            'bs_shareholder_loans_fp1' => ['required', 'max:10'], 'bs_capital_expenditure_py3' => ['required', 'max:10'],
            'bs_capital_expenditure_py2' => ['required', 'max:10'], 'bs_capital_expenditure_py1' => ['required', 'max:10'],
            'bs_capital_expenditure_ytd' => ['required', 'max:10'], 'bs_capital_expenditure_fp5' => ['required', 'max:10'],
            'bs_capital_expenditure_fp4' => ['required', 'max:10'], 'bs_capital_expenditure_fp3' => ['required', 'max:10'],
            'bs_capital_expenditure_fp2' => ['required', 'max:10'], 'bs_capital_expenditure_fp1' => ['required', 'max:10']
        ]);


        //GP Margin >= 50%
        $gp_margin = $this->gp_margin($request);
        $mean_gp_margin = (array_sum($gp_margin) / 4);
        $final_gp_margin = number_format($mean_gp_margin, 1);

        //EBITDA Margin >= 15%
        $ebitda_margin = $this->ebitda_margin($request);
        $mean_ebitda_margin = (array_sum($ebitda_margin) / 4);
        $final_ebitda_margin = number_format($mean_ebitda_margin, 1);

        //PAT Margin >= 10%
        $pat_margin = $this->pat_margin($request);
        $mean_pat_margin = (array_sum($pat_margin) / 4);
        $final_pat_margin = number_format($mean_pat_margin, 1);

        if ($valid->fails()) {
            return view('forms.financial')->with('form_applicants', $form_applicants)
                ->with('form_compliants', $form_compliants)
                ->with('form_backgrounds', $form_backgrounds)
                ->withErrors($valid);
        }


        if (isset($_POST['submit'])) {

            if (session()->has('session_financials')) {
                $session_financials = session()->get('session_financials');
                $request->session()->flash('info_message', 'ATTENTION! You have an incomplete registration you can continue filling it below');

                if ($final_gp_margin >= 50 && $final_ebitda_margin >= 15 && $final_pat_margin >= 10) {

                    $this->submit_applicant($request);
                    $this->submit_compliance($request);
                    $this->submit_background($request);
                    $this->submit_financial($request);
                    $this->submit_tier1_keyratios($request);
                    $request->session()->forget('session_applicants');
                    $request->session()->forget('session_compliants');
                    $request->session()->forget('session_backgrounds');
                    $request->session()->forget('session_financials');
                    $request->session()->flash('success_message', 'Refiner Submitted Successfully');

                    //successful Notification for successful Submission
                    $applicant = $request->email;
                    $details = [
                        'greeting' => 'Hello!',
                        'subject' > 'TIA360 Registration',
                        'body' => 'You have successfully Registered with TIA360 and Submitted your Refiner. We will get in touch with you with more details.',
                        'thanks' => 'Thank you for signing up with TIA360!'
                    ];

                    Notification::route('mail', $applicant)->notify(new RefinerSubmitted($details));
                    return redirect('/');
                } else {
                    $this->submit_applicant($request);
                    $this->submit_compliance($request);
                    $this->submit_background($request);
                    $this->submit_financial($request);
                    $this->submit_tier2_keyratios($request);
                    $request->session()->forget('session_applicants');
                    $request->session()->forget('session_compliants');
                    $request->session()->forget('session_backgrounds');
                    $request->session()->forget('session_financials');
                    $request->session()->flash('success_message', 'Refiner Submitted Successfully');

                    //successful Notification for successful Submission
                    $details = [
                        'greeting' => 'Hello!',
                        'subject' > 'TIA360 Registration',
                        'body' => 'You have successfully Registered with TIA360 and Submitted your Refiner. We will get in touch with you with more details.',
                        'thanks' => 'Thank you for signing up with TIA360!'
                    ];

                    Notification::route('mail', $request->email)->notify(new RefinerSubmitted($details));
                    Notification::route('mail', 'tourism@tia360.africa')->notify(new RefinerSubmitted($details));
                    return redirect('/');
                }
            }

            if ($final_gp_margin >= 50 && $final_ebitda_margin >= 15 && $final_pat_margin >= 10) {

                $this->submit_applicant($request);
                $this->submit_compliance($request);
                $this->submit_background($request);
                $this->submit_financial($request);
                $this->submit_tier1_keyratios($request);
                $request->session()->forget('session_applicants');
                $request->session()->forget('session_compliants');
                $request->session()->forget('session_backgrounds');
                $request->session()->forget('session_financials');
                $request->session()->flash('success_message', 'Refiner Submitted Successfully');

                //successful Notification for successful Submission
                $applicant = $request->email;
                $details = [
                    'greeting' => 'Hello!',
                    'subject' > 'TIA360 Registration',
                    'body' => 'You have successfully Registered with TIA360 and Submitted your Refiner. We will get in touch with you with more details.',
                    'thanks' => 'Thank you for signing up with TIA360!'
                ];

                Notification::route('mail', $applicant)->notify(new RefinerSubmitted($details));
                return redirect('/');
            } else {
                $this->submit_applicant($request);
                $this->submit_compliance($request);
                $this->submit_background($request);
                $this->submit_financial($request);
                $this->submit_tier2_keyratios($request);
                $request->session()->forget('session_applicants');
                $request->session()->forget('session_compliants');
                $request->session()->forget('session_backgrounds');
                $request->session()->forget('session_financials');
                $request->session()->flash('success_message', 'Refiner Submitted Successfully');

                //successful Notification for successful Submission
                $applicant = $request->email;
                $details = [
                    'greeting' => 'Hello!',
                    'subject' > 'TIA360 Registration',
                    'body' => 'You have successfully Registered with TIA360 and Submitted your Refiner. We will get in touch with you with more details.',
                    'thanks' => 'Thank you for signing up with TIA360!'
                ];

                Notification::route('mail', $applicant)->notify(new RefinerSubmitted($details));
                //Notification::send($user, new RefinerSubmitted($details));
                return redirect('/');
            }
        }

        if (isset($_POST['save'])) {
            session()->put('session_applicants', $form_applicants);
            session()->put('session_compliants', $form_compliants);
            session()->put('session_backgrounds', $form_backgrounds);
            session()->put('session_financials', $form_financials);
            $request->session()->flash('success_message', 'Registration saved for Later, You can complete registration later.');
            return redirect('/');
        }
    }
}
