<?php

namespace App\Http\Controllers;

use App\Exports\ReportsExport;

class ExportController extends Controller
{
    /**
     * export applicants details into an excel file
     *
     * @param int $id
     * @return ReportsExport
     **/
    public function export($id)
    {
        return (new ReportsExport($id))->download('tia360_applicant.xlsx');
    }
}
