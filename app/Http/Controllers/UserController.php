<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\StaffSubmitted;
use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_email = User::where('email', $request->email)->value('email');
        if ($user_email) {
            $request->session()->flash('error_message', 'User with Similar email Exists!');
            return redirect('all-users');
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->role = $request->role;

            $password = $request->name . '@_123';
            $user->password = bcrypt($password);
            $user->save();

            //successful Notification for successful Submission
            $applicant = $request->email;
            $details = [
                'greeting' => 'Hello!',
                'subject' > 'TIA360 Registration',
                'body' => "You Credentials are Email: ' . $user->email . 'and you password is ' . $password ",
                'thanks' => 'Thank you for using our signing up with TIA360!'
            ];

            Notification::send($user, new StaffSubmitted($details));
            return redirect('all-users');
            $request->session()->flash('success_message', 'User Added Successful');
            return redirect('all-users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show')
            ->with('users', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = $request->role;

        $password = $request->password;
        $user->password = bcrypt($password);
        $user->save();
        $request->session()->flash('success_message', 'Update Successful');
        return redirect('all-users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        flash()->success('User Deleted Successfully');
        return redirect('all-users');
    }
}
