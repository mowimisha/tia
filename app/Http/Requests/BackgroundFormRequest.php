<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BackgroundFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shareholder_name' => ['required', 'string', 'max:255'],
            'shareholder_id' => ['required', 'string', 'max:255'],
            'entity_name' => ['required', 'string', 'max:255'],
            'entity_registration_number' => ['required', 'string', 'max:255'],
            'business_status' => ['required', 'string', 'max:255'],
            'individual_shareholder_percentage' => ['required', 'string', 'max:255'],
            'entity_shareholder_percentage' => ['required', 'string', 'max:255'],
            'management_company_name' => ['required', 'string', 'max:255'],
            'management_registration_number' => ['required', 'string', 'max:255'],
            'management_company_position' => ['required', 'string', 'max:255'],
            'permanent_staff_no' => ['required', 'string', 'max:255'],
            'temporary_staff_no' => ['required', 'string', 'max:255'],
            'business_location_province' => ['required', 'string', 'max:255'],
            'business_nearest_city_location' => ['required', 'string', 'max:255'],
            'project_actual_location_area' => ['required', 'string', 'max:255'],
            'business_description' => ['required', 'string', 'max:255'],
            'business_goto_strategy' => ['required', 'string', 'max:255'],
            'business_competitive_advantage' => ['required', 'string', 'max:255'],
            'business_areas_of_improvement' => ['required', 'string', 'max:255'],
            'business_strategic_opportunities' => ['required', 'string', 'max:255'],
            'business_financial_year_end' => ['required', 'string', 'max:255'],
            'business_accountant_name' => ['required', 'string', 'max:255']
        ];
    }
}
