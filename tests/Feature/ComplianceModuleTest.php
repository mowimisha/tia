<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ComplianceModuleTest extends TestCase
{
    /**
     * @test
     *
     **/
    public function applicant_can_view_add_applicants_form()
    {
        $response = $this->get(view('form.compliance'));
        $response->assertStatus(200);
        $response->assertViewIs('forms.compliance');
        $response->assertViewHas('firstname', 'lastname', 'race', 'nationality', 'email', 'phone', 'business_status');
    }

    /**
     * @test
     *
     **/
    public function applicant_can_submit_details_and_view_background_form()
    {
        $applicant = factory(Applicant::class)->raw();
        $this->followingRedirects()->post(route('forms.compliance'), $applicant)->assertStatus(200);

        $this->get(route('forms.background'))
            ->assertRedirect(route('forms.background'));
    }
}
