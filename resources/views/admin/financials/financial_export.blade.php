
<table>
    @foreach($financials as $financial)
        <tbody>
            <tr>
                <td><strong>Income Statement Overview</strong></td>
                <td><strong>PY -3</strong></td>
                <td><strong>PY -2</strong></td>
                <td><strong>PY -1</strong></td>
                <td><strong>YTD</strong></td>
                <td><strong>FP 1</strong></td>
                <td><strong>FP 2</strong></td>
                <td><strong>FP 3</strong></td>
                <td><strong>FP 4</strong></td>
                <td><strong>FP 5</strong></td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Revenue</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_revenue_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Cost of Sales</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_cost_of_sales_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Gross Profit</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_gross_profit_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Operating Expenses</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_operating_expenses_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Profit Before Tax</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_before_tax_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Tax</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_tax_fp5 }} </h5>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Profit After Tax</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_profit_after_tax_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Depreciation</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_depreciation_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Interest Expense</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->is_interest_expense_fp5 }} </h5>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>

<table>
    @foreach($financials as $financial)
        <tbody>
            <tr>
                <td><strong>Balance Sheet Figures</strong></td>
                <td><strong>PY -3</strong></td>
                <td><strong>PY -2</strong></td>
                <td><strong>PY -1</strong></td>
                <td><strong>YTD</strong></td>
                <td><strong>FP 1</strong></td>
                <td><strong>FP 2</strong></td>
                <td><strong>FP 3</strong></td>
                <td><strong>FP 4</strong></td>
                <td><strong>FP 5</strong></td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Total Assets</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_assets_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Fixed Assets</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_fixed_assets_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Current Assets</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_assets_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Total Liabilities</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_total_liabilities_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Long Term Liabilities</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_longterm_liabilities_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Current Liabilities</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_current_liabilities_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Equity</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_equity_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Shareholder Loans</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_shareholder_loans_fp5 }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Capital Expenditure</label>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_py3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_py2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_py1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_ytd }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_fp1 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_fp2 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_fp3 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_fp4 }} </h5>
                </td>
                <td>
                    <h5 class="text-info"> {{ $financial->bs_capital_expenditure_fp5 }} </h5>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>

































