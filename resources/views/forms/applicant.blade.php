@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if (session('success_message'))
                    <div class="alert alert-success font-weight-bold">{{ session('success_message') }}</div>
                @elseif (session('error_message'))
                    <div class="alert alert-danger font-weight-bold">{{ session('error_message') }}</div>
                @elseif (session('warning_message'))
                    <div class="alert alert-warning font-weight-bold">{{ session('warning_message') }}</div>
                @elseif (session('info_message'))
                    <div class="alert alert-info font-weight-bold">{{ session('info_message') }}</div>
                @else
                @endif

                <div class="card">
                    <div class="card-header bg-primary text-white font-weight-bold text-uppercase rounded-0 pt-3 pb-3">{{ __('APPLICANT REGISTRATION') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('applicant-register') }}" class="pr-5 pl-5" autocomplete="off">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="firstname" class="col-form-label font-weight-bold text-muted">{{ __('FirstName') }}</label>
                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <input id="firstname" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('firstname') is-invalid @enderror" name="firstname" value="{{ $applicant['firstname'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="firstname" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" autofocus>
                                    @endif

                                    @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="lastname" class="col-form-label font-weight-bold text-muted">{{ __('Lastname') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <input id="lastname" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('lastname') is-invalid @enderror" name="lastname" value="{{ $applicant['lastname'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="lastname" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" autofocus>
                                    @endif

                                    @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="race" class="col-form-label font-weight-bold text-muted">{{ __('Race') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <select name="race" id="race" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('race') is-invalid @enderror" autofocus>
                                                <option value="{{ $applicant['race'] }}" selected>{{ $applicant['race'] }}</option>
                                                @foreach ($races as $race)
                                                    <option value="" hidden>Select Race</option>
                                                    <option value="{{ $race }}" {{ old('race') == $race ? "selected" :""}}>{{ $race }}</option>
                                                @endforeach
                                            </select>
                                        @endforeach
                                    @else
                                        <select name="race" id="race" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('race') is-invalid @enderror" autofocus>
                                            @foreach ($races as $race)
                                                <option value="" hidden>Select Race</option>
                                                <option value="{{ $race }}" {{ old('race') == $race ? "selected" :""}}>{{ $race }}</option>
                                            @endforeach
                                        </select>
                                    @endif

                                    @error('race')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label for="nationality" class="col-form-label font-weight-bold text-muted">{{ __('Nationality') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <input id="nationality" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('nationality') is-invalid @enderror" name="nationality" value="{{ $applicant['nationality'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="nationality" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('nationality') is-invalid @enderror" name="nationality" value="{{ old('nationality') }}">
                                    @endif

                                    @error('nationality')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="email" class="col-form-label font-weight-bold text-muted">{{ __('E-Mail Address') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <input id="email" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('email') is-invalid @enderror" name="email" value="{{ $applicant['email'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="email" type="email" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                                    @endif

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label for="phone" class="col-form-label font-weight-bold text-muted">{{ __('Phone Number') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <input id="phone" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('phone') is-invalid @enderror" name="phone" value="{{ $applicant['phone'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="phone" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}">
                                    @endif

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="business_status" class="col-form-label font-weight-bold text-muted">{{ __('Business Status') }}</label>

                                    @if (Session::has('session_applicants'))
                                        @foreach ($session_applicants as $applicant)
                                            <select name="business_status" id="business_status" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('business_status') is-invalid @enderror"  autofocus>
                                                <option value="{{ $applicant['business_status'] }}" selected> {{ $applicant['business_status'] == 0 ? 'Start Up' : 'Existing Business' }}</option>
                                                @foreach ($status as $state)
                                                    <option value="{{ $state }}"> {{ $state == 0 ? 'Start Up' : 'Existing Business' }}</option>
                                                @endforeach
                                            </select>
                                        @endforeach
                                    @else
                                        <select name="business_status" id="business_status" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('business_status') is-invalid @enderror"  autofocus>
                                            @foreach ($status as $state)
                                                <option value="" hidden> Select Business Status </option>
                                                <option value="{{ $state }}" {{ old('business_status') == $state ? "selected" :""}}> {{ $state == 0 ? 'Start Up' : 'Existing Business' }}</option>
                                            @endforeach
                                        </select>
                                    @endif

                                    @error('business_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                                <div class="col-md-6"></div>
                            </div>

                            <div class="form-group row pt-3">
                                <div class="col-md-8">
                                    <button type="submit" name="submit" class="btn btn-primary font-weight-bold pr-5 pl-5 mr-3">
                                        {{ __('Submit') }}
                                    </button>
                                    <button type="submit" name="save" class="btn btn-outline-primary font-weight-bold pr-5 pl-5">
                                        {{ __('Save for Later') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
