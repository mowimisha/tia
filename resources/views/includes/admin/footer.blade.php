
        </div>
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<script>
			$('.carousel').carousel({
				pause: "false"
			});
        </script>
        <script>
            $("document").ready(function(){
                setTimeout(function(){
                $("div.alert").remove();
                }, 3000 );
            });
        </script>

		<script>
			$(document).ready(function() {
				$('#table').DataTable({
					responsive: true,
                    lengthChange: true,
                    "order": [[ 0, "desc" ]],

					buttons: [],
				});
			});
		</script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <script src="../admin/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="../admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/input-mask.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
		<script src="../admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
        <script src="../admin/assets/demo/default/custom/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
        <script src="../admin/assets/js/demo1/pages/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
		<script src="../admin/assets/app/js/dashboard.js" type="text/javascript"></script>
        <script src="../admin/assets/demo/default/custom/crud/wizard/wizard.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
        <script src="../admin/assets/app/js/bootstrap-datepicker.init.js" type="text/javascript"></script>
        <script src="../admin/assets/app/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="../admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

	</body>
	<!-- end::Body -->
</html>
