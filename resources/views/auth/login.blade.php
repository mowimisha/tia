@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">

                {{-- @if (session('error_message'))
                    <div class="alert alert-danger font-weight-bold">{{ session('error_message') }}</div>
                @elseif (session('warning_message'))
                    <div class="alert alert-warning font-weight-bold">{{ session('warning_message') }}</div>
                @elseif (session('info_message'))
                    <div class="alert alert-info font-weight-bold">{{ session('info_message') }}</div>
                @elseif (session('success_message'))
                    <div class="alert alert-success font-weight-bold">{{ session('success_message') }}</div>
                @endif --}}


                <div class="card">
                    <div class="card-header bg-primary text-white text-uppercase font-weight-bold rounded-0 pb-3">{{ __('Login') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('login') }}" autocomplete="off">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <label for="email" class="col-form-label font-weight-bold">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <label for="password" class="col-form-label font-weight-bold">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('password') is-invalid @enderror" name="password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- <div class="form-group row">
                                <div class="col-md-6 offset-md-2">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="form-group row pt-3">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-primary text-uppercase font-weight-bold pr-5 pl-5">
                                        {{ __('Login') }}
                                    </button>

                                    {{-- @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
